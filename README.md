Use the links on this page to download the latest version of Prolific USB-to-Serial Comm Port drivers. All drivers available for download have been scanned by antivirus program. Please choose the relevant version according to your computer's operating system and click the download button.
Driver Version: 3.4.48.272
Release Date: 2013-02-05
File Size: 71.35K
Supported OS: Windows 10 64 bit, Windows 8.1 64bit, Windows 7 64bit, Windows Vista 64bit, Windows XP 64bit